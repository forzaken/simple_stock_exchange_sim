package co.task

import java.io.PrintWriter

import cats.data.Validated.{ Invalid, Valid }
import co.task.parser.Parser
import co.task.parser.RawDataSource.LocalFileSource

object Main extends App {
  require(args.length == 3)
  val clientsSrc = LocalFileSource(args(0))
  val ordersSrc = LocalFileSource(args(1))
  val parser = new Parser(clientsSrc, ordersSrc)
  val validator = new OrdersProcessingValidator
  val simpleStockExchangeSimulator = new SimpleStockExchangeSimulator()
  parser.parse() match {
    case Valid(pr) =>
      val updClients = simpleStockExchangeSimulator.processOrders(pr.clients, pr.orders)
      validator.validateClients(pr.clients, updClients) match {
        case Valid(_) =>
          val updClientsStr = {
            updClients.map(x => {
              s"${x.name}\t${x.dollarBalance}\t" +
                s"${x.getStockBalance("A")}\t${x.getStockBalance("B")}\t" +
                s"${x.getStockBalance("C")}\t${x.getStockBalance("D")}"
            }).mkString("\n")
          }
          new PrintWriter(args(2)) { write(updClientsStr); close() }

        case Invalid(errors) =>
          val msg = s"[${errors.toList.mkString(",")}]"
          throw new Exception(msg)
      }
    case Invalid(errors) =>
      val msg = s"[${errors.toList.mkString(",")}]"
      throw new Exception(msg)
  }
}
