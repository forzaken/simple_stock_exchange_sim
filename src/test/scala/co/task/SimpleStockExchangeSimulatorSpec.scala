package co.task

import co.task.entities.{ Client, Order, OrderType }

class SimpleStockExchangeSimulatorSpec extends BaseSpec {

  "simple stock exchange" must {
    val simulator = new SimpleStockExchangeSimulator()
    val clients = Map(
      "C1" -> Client("C1", dollarBalance     = 10, stockNameToAmount = Map("A" -> 5)),
      "C2" -> Client("C2", dollarBalance     = 15, stockNameToAmount = Map("A" -> 5))
    )

    "perform exchange [1] (simple exchange)" in {
      val buyOrder = Order(
        clientName = "C1", orderType = OrderType.Buy,
        stockName  = "A", price = 2, amount = 2
      )
      val sellOrder = Order(
        clientName = "C2", orderType = OrderType.Sell,
        stockName  = "A", price = 2, amount = 2
      )
      val (updatedClients, updatedSellOrd, updatedBuyOrd) =
        simulator.performExchange(clients, sellOrder, buyOrder)

      val clientsList = updatedClients.toList.map(_._2)
      val client1 = clientsList(0)
      val client2 = clientsList(1)
      client1.getStockBalance("A") shouldBe 7
      client1.dollarBalance shouldBe 6
      client2.getStockBalance("A") shouldBe 3
      client2.dollarBalance shouldBe 19
      updatedBuyOrd.amount shouldBe 0
      updatedBuyOrd.orderType.isBuy shouldBe true
      updatedSellOrd.amount shouldBe 0
      updatedSellOrd.orderType.isSell shouldBe true
    }

    "perform exchange [2] (buyer partial exchange)" in {
      val buyOrder = Order(
        clientName = "C1", orderType = OrderType.Buy,
        stockName  = "A", price = 2, amount = 3
      )
      val sellOrder = Order(
        clientName = "C2", orderType = OrderType.Sell,
        stockName  = "A", price = 2, amount = 2
      )
      val (updatedClients, updatedBuyOrd, updatedSellOrd) =
        simulator.performExchange(clients, buyOrder, sellOrder)

      val clientsList = updatedClients.toList.map(_._2)
      val client1 = clientsList(0)
      val client2 = clientsList(1)

      client1.getStockBalance("A") shouldBe 7
      client1.dollarBalance shouldBe 6
      client2.getStockBalance("A") shouldBe 3
      client2.dollarBalance shouldBe 19
      updatedBuyOrd.amount shouldBe 1
      updatedBuyOrd.orderType.isBuy shouldBe true
      updatedSellOrd.amount shouldBe 0
      updatedSellOrd.orderType.isSell shouldBe true
    }

    "perform exchange [3] (seller partial exchange)" in {
      val buyOrder = Order(
        clientName = "C1", orderType = OrderType.Buy,
        stockName  = "A", price = 2, amount = 1
      )
      val sellOrder = Order(
        clientName = "C2", orderType = OrderType.Sell,
        stockName  = "A", price = 2, amount = 2
      )
      val (updatedClients, updatedBuyOrd, updatedSellOrd) =
        simulator.performExchange(clients, buyOrder, sellOrder)

      val clientsList = updatedClients.toList.map(_._2)
      val client1 = clientsList(0)
      val client2 = clientsList(1)

      client1.getStockBalance("A") shouldBe 6
      client1.dollarBalance shouldBe 8
      client2.getStockBalance("A") shouldBe 4
      client2.dollarBalance shouldBe 17
      updatedBuyOrd.amount shouldBe 0
      updatedBuyOrd.orderType.isBuy shouldBe true
      updatedSellOrd.amount shouldBe 1
      updatedSellOrd.orderType.isSell shouldBe true
    }

    "perform exchange [4] (do nothing in case if customer has insufficient balance)" in {
      val buyOrder = Order(
        clientName = "C1", orderType = OrderType.Buy,
        stockName  = "A", price = 2, amount = 10
      )
      val sellOrder = Order(
        clientName = "C2", orderType = OrderType.Sell,
        stockName  = "A", price = 2, amount = 2
      )
      val (updatedClients, updatedBuyOrd, updatedSellOrd) =
        simulator.performExchange(clients, buyOrder, sellOrder)

      val clientsList = updatedClients.toList.map(_._2)
      val client1 = clientsList(0)
      val client2 = clientsList(1)

      client1.getStockBalance("A") shouldBe 5
      client1.dollarBalance shouldBe 10
      client2.getStockBalance("A") shouldBe 5
      client2.dollarBalance shouldBe 15
      updatedBuyOrd.amount shouldBe 10
      updatedBuyOrd.orderType.isBuy shouldBe true
      updatedSellOrd.amount shouldBe 2
      updatedSellOrd.orderType.isSell shouldBe true
    }

    "perform exchange [5] (do nothing in case if seller has insufficient amount of stocks)" in {
      val buyOrder = Order(
        clientName = "C1", orderType = OrderType.Buy,
        stockName  = "A", price = 2, amount = 1
      )
      val sellOrder = Order(
        clientName = "C2", orderType = OrderType.Sell,
        stockName  = "A", price = 2, amount = 10
      )
      val (updatedClients, updatedBuyOrd, updatedSellOrd) =
        simulator.performExchange(clients, buyOrder, sellOrder)

      val clientsList = updatedClients.toList.map(_._2)
      val client1 = clientsList(0)
      val client2 = clientsList(1)

      client1.getStockBalance("A") shouldBe 5
      client1.dollarBalance shouldBe 10
      client2.getStockBalance("A") shouldBe 5
      client2.dollarBalance shouldBe 15
      updatedBuyOrd.amount shouldBe 1
      updatedBuyOrd.orderType.isBuy shouldBe true
      updatedSellOrd.amount shouldBe 10
      updatedSellOrd.orderType.isSell shouldBe true
    }

    "perform exchange [5] (do nothing in case if it's self-to-self order)" in {
      val buyOrder = Order(
        clientName = "C1", orderType = OrderType.Buy,
        stockName  = "A", price = 2, amount = 1
      )
      val sellOrder = Order(
        clientName = "C1", orderType = OrderType.Sell,
        stockName  = "A", price = 2, amount = 1
      )
      val (updatedClients, updatedBuyOrd, updatedSellOrd) =
        simulator.performExchange(clients, buyOrder, sellOrder)

      val clientsList = updatedClients.toList.map(_._2)
      val client1 = clientsList(0)
      val client2 = clientsList(1)

      client1.getStockBalance("A") shouldBe 5
      client1.dollarBalance shouldBe 10
      client2.getStockBalance("A") shouldBe 5
      client2.dollarBalance shouldBe 15
      updatedBuyOrd.amount shouldBe 1
      updatedBuyOrd.orderType.isBuy shouldBe true
      updatedSellOrd.amount shouldBe 1
      updatedSellOrd.orderType.isSell shouldBe true
    }

    "process single order [1] (buy order)" in {
      val buyOrder = Order(
        clientName = "C1", orderType = OrderType.Buy,
        stockName  = "A", price = 2, amount = 4
      )
      val orders = Vector(
        Order(
          clientName = "C2", orderType = OrderType.Sell,
          stockName  = "A", price = 2, amount = 1
        ),
        Order(
          clientName = "C2", orderType = OrderType.Sell,
          stockName  = "A", price = 2, amount = 1
        ),
        Order(
          clientName = "C2", orderType = OrderType.Sell,
          stockName  = "A", price = 2, amount = 1
        )
      )
      val (updOrders, updClients) = {
        simulator.processSingleOrder(buyOrder, clients, orders)
      }
      updOrders.forall(_.amount == 0) shouldBe true
      val updClientsList = updClients.values.toList
      updClientsList(0).dollarBalance shouldBe 4
      updClientsList(1).dollarBalance shouldBe 21
    }

    "process single order [2] (sell order)" in {
      val buyOrder = Order(
        clientName = "C1", orderType = OrderType.Sell,
        stockName  = "A", price = 2, amount = 4
      )
      val orders = Vector(
        Order(
          clientName = "C2", orderType = OrderType.Buy,
          stockName  = "A", price = 2, amount = 1
        ),
        Order(
          clientName = "C2", orderType = OrderType.Sell,
          stockName  = "A", price = 3, amount = 1
        ),
        Order(
          clientName = "C2", orderType = OrderType.Buy,
          stockName  = "A", price = 2, amount = 1
        )
      )
      val (updOrders, updClients) = {
        simulator.processSingleOrder(buyOrder, clients, orders)
      }
      updOrders.filter(_.orderType.isBuy).forall(_.amount == 0) shouldBe true
      updOrders.filter(_.orderType.isSell).forall(_.amount == 1) shouldBe true
      val updClientsList = updClients.values.toList
      updClientsList(0).dollarBalance shouldBe 14
      updClientsList(1).dollarBalance shouldBe 11
    }

    "process orders and update clients wallet [1]" in {
      val c1buy = Order(
        clientName = "C1", orderType = OrderType.Buy,
        stockName  = "A", price = 1, amount = 1
      )
      val c1sell = Order(
        clientName = "C1", orderType = OrderType.Sell,
        stockName  = "A", price = 1, amount = 1
      )
      val c2buy = Order(
        clientName = "C2", orderType = OrderType.Buy,
        stockName  = "A", price = 1, amount = 1
      )
      val c2sell = Order(
        clientName = "C2", orderType = OrderType.Sell,
        stockName  = "A", price = 1, amount = 1
      )
      //nothing should change
      val orders = Seq(
        c1buy, c1buy, c1buy,
        c2sell, c2sell, c2sell,
        c1sell, c1sell, c1sell,
        c2buy, c2buy, c2buy
      )
      val updClients = simulator.processOrders(clients.values.toSeq, orders)
      updClients(0).dollarBalance shouldBe 10
      updClients(1).dollarBalance shouldBe 15
    }

    "process orders and update clients wallet [2]" in {
      val c1buy = Order(
        clientName = "C1", orderType = OrderType.Buy,
        stockName  = "A", price = 1, amount = 1
      )
      val c2sell = Order(
        clientName = "C2", orderType = OrderType.Sell,
        stockName  = "A", price = 1, amount = 1
      )
      //nothing should change
      val orders = Seq(
        c1buy, c1buy, c1buy,
        c1buy, c1buy, c1buy,
        c2sell, c2sell, c2sell
      )
      val updClients = simulator.processOrders(clients.values.toSeq, orders)
      updClients(0).dollarBalance shouldBe 7
      updClients(0).getStockBalance("A") shouldBe 8
      updClients(1).dollarBalance shouldBe 18
      updClients(1).getStockBalance("A") shouldBe 2
    }
  }
}
