package co.task.entities

case class Order(
  clientName: String,
  orderType:  OrderType,
  stockName:  String,
  price:      Int,
  amount:     Int
) {
  def decreaseAmount(cnt: Int) = {
    this.copy(amount = this.amount - cnt)
  }

  def increaseAmount(cnt: Int) = {
    this.copy(amount = this.amount + cnt)
  }
}

sealed trait OrderType {
  def isBuy: Boolean
  def isSell: Boolean
}

object OrderType {
  case object Buy extends OrderType {
    override def isBuy = true
    override def isSell = false
  }

  case object Sell extends OrderType {
    override def isBuy = false
    override def isSell = true
  }

  def fromString(str: String): OrderType = str match {
    case "b" => Buy
    case "s" => Sell
    case _   => throw new Exception(s"""unrecognized order type, expected: ["b" - buy, "s" - sell], actual: $str""")
  }
}
