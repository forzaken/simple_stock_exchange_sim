package co.task.parser

trait RawDataSource {
  def getRawData: String
}

object RawDataSource {
  case class LocalFileSource(localPath: String) extends RawDataSource {
    override def getRawData = scala.io.Source.fromFile(localPath).mkString
  }

  case class StringSource(rawData: String) extends RawDataSource {
    override def getRawData() = rawData
  }
}
