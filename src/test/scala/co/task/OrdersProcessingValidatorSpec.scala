package co.task

import co.task.entities.Client

class OrdersProcessingValidatorSpec extends BaseSpec {
  val validator = new OrdersProcessingValidator

  "orders processing validator" must {
    "validate processing result [1] (success case)" in {
      val clients = Seq(
        Client("C1", 1, Map("A" -> 1, "B" -> 1, "C" -> 1, "D" -> 1)),
        Client("C2", 2, Map("A" -> 1, "B" -> 1, "C" -> 1, "D" -> 1))
      )
      validator.validateClients(clients, clients).isValid shouldBe true
    }

    "validate processing result [2] (failed case)" in {
      val clients = Seq(
        Client("C1", 1, Map("A" -> 1, "B" -> 1, "C" -> 1, "D" -> 1)),
        Client("C2", 2, Map("A" -> 1, "B" -> 1, "C" -> 1, "D" -> 1))
      )
      val processedClients = Seq(
        Client("C1", 1, Map("A" -> 1, "B" -> 2, "C" -> 1, "D" -> 1)),
        Client("C2", 2, Map("A" -> 2, "B" -> 1, "C" -> 1, "D" -> 1))
      )
      validator.validateClients(clients, processedClients).toEither
        .left.get.toList.size shouldBe 2
    }
  }
}
