import sbt._

object Dependencies {
  private val catsVersion = "1.0.1"
  private val scalatestVersion = "3.0.1"

  val dependencies = {
    val cats = Seq(
      "org.typelevel" %% "cats-core" % catsVersion
    )

    val scalatest = Seq(
      "org.scalatest" %% "scalatest" % scalatestVersion % Test
    )

    cats ++ scalatest
  }
}
