package co.task

import org.scalatest.{ BeforeAndAfterAll, Matchers, WordSpec }

trait BaseSpec extends WordSpec with BeforeAndAfterAll with Matchers
