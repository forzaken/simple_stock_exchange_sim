package co.task.parser

import cats.data.ValidatedNel
import cats.implicits._
import co.task.entities._
import Parser._
import co.task.Monoids._

import scala.util.{ Failure, Success, Try }

class Parser(clientsSource: RawDataSource, ordersSource: RawDataSource) {

  private def parseLines[T](
    rawDataSource: RawDataSource,
    parseLine:     (Int, String) => ValidatedNel[ParsingError, T]
  ): ValidatedNel[ParsingError, Seq[T]] = {
    rawDataSource.getRawData.split("\n").filterNot(_.isEmpty).zipWithIndex.map {
      case (str, line) =>
        parseLine(line, str).map(x => Seq[T](x))
    }.toList.combineAll
  }

  def parseClients(): ValidatedNel[ParsingError, Seq[Client]] = {
    def parseLine(line: Int, data: String) = {
      Try {
        data.split("\t").toList match {
          case List(clientName, dollars, a, b, c, d) =>
            val stockNameToAmount = Map(
              "A" -> a.toInt, "B" -> b.toInt, "C" -> c.toInt, "D" -> d.toInt
            )
            Client(clientName, dollars.toInt, stockNameToAmount).validNel
          case _ =>
            ParsingError(line + 1, "inconsistent row number", "clients").invalidNel
        }
      } match {
        case Success(v) => v
        case Failure(e) => ParsingError(line + 1, e.getMessage, "clients").invalidNel
      }
    }

    parseLines(clientsSource, parseLine)
  }

  def parseOrders(): ValidatedNel[ParsingError, Seq[Order]] = {
    def parseLine(line: Int, data: String) = {
      Try {
        data.split("\t").toList match {
          case List(clientName, orderType, stockName, price, amount) =>
            Order(
              clientName, OrderType.fromString(orderType), stockName, price.toInt, amount.toInt
            ).validNel
          case _ =>
            ParsingError(line + 1, "inconsistent row number", "orders").invalidNel
        }
      } match {
        case Success(v) => v
        case Failure(e) => ParsingError(line + 1, e.getMessage, "orders").invalidNel
      }
    }

    parseLines(ordersSource, parseLine)
  }

  def parse(): ValidatedNel[ParsingError, ParsingResult] = {
    (parseClients(), parseOrders()).mapN(ParsingResult)
  }
}

object Parser {
  case class ParsingError(line: Int, msg: String, domain: String) {
    override def toString = s"|invalid input at line - [$line], message - [$msg], domain - [$domain]|"
  }

  case class ParsingResult(clients: Seq[Client], orders: Seq[Order])
}
