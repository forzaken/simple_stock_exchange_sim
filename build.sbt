import scalariform.formatter.preferences._

name := "spent_assignment"

version := "0.1"

scalaVersion := "2.11.11"

lazy val root = project
  .in(file("."))
  .settings(
    mainClass in Compile := Some("co.task.Main"),
    libraryDependencies ++= Dependencies.dependencies,
    scalacOptions ++= Seq(
      "-Ywarn-dead-code",
      "-Ywarn-inaccessible",
      "-encoding", "utf-8",
      "-Ypartial-unification"
    )
  )

scalariformPreferences := scalariformPreferences.value
  .setPreference(DanglingCloseParenthesis, Force)
  .setPreference(AlignArguments, true)
  .setPreference(AlignParameters, true)
  .setPreference(AlignSingleLineCaseStatements, true)
  .setPreference(DanglingCloseParenthesis, Force)
  .setPreference(FirstParameterOnNewline, Force)
