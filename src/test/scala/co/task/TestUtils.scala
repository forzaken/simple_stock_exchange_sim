package co.task

trait TestUtils {
  implicit class UnsafeOptionExt[+T](opt: Option[T]) {
    def getMandatory = {
      opt.getOrElse(throw new Exception("option is empty"))
    }
  }
}

object TestUtils extends TestUtils
