package co.task

import cats.implicits._
import cats.data.ValidatedNel
import co.task.entities.Client
import co.task.OrdersProcessingValidator.OrdersValidationError

class OrdersProcessingValidator {

  private def getAggregatedBalance(clients: Seq[Client]) = {
    def mergeMaps(map1: Map[String, Int], map2: Map[String, Int]): Map[String, Int] = {
      (map1.toSeq ++ map2.toSeq).groupBy(_._1).map { case (name, seq) => name -> seq.map(_._2).sum }
    }

    clients.map(x => x.stockNameToAmount ++ Map("Dollars" -> x.dollarBalance))
      .foldLeft(Map.empty[String, Int]) {
        case (acc, stockToAmount) =>
          mergeMaps(stockToAmount, acc)
      }
  }

  def validateClients(initialClients: Seq[Client], processedClients: Seq[Client]): ValidatedNel[OrdersValidationError, Unit] = {
    val before = getAggregatedBalance(initialClients)
    val after = getAggregatedBalance(processedClients)
    before.map {
      case (token, amount) =>
        after.get(token) match {
          case Some(x) =>
            if (amount == x) {
              ().validNel
            } else {
              OrdersValidationError(s"token $token has incorrect aggregation sum [before: $amount, after: $x]")
                .invalidNel
            }
          case None =>
            OrdersValidationError(s"token $token was not found for processed clients")
              .invalidNel
        }
    }.toList.combineAll
  }
}

object OrdersProcessingValidator {
  case class OrdersValidationError(msg: String)
}
