package co.task

import cats.Monoid
import cats.data.Validated.{ Invalid, Valid }
import cats.data.ValidatedNel
import cats.implicits._

object Monoids {

  implicit def aggregateMonoid[A, T] = new Monoid[ValidatedNel[A, Seq[T]]] {
    override def empty = Seq.empty[T].validNel

    override def combine(x: ValidatedNel[A, Seq[T]], y: ValidatedNel[A, Seq[T]]) = {
      (x, y) match {
        case (Valid(a), Valid(b)) =>
          (a ++ b).validNel
        case (Invalid(a), Invalid(b)) =>
          a.combine(b).invalid[Seq[T]]
        case (Invalid(a), _) =>
          a.invalid[Seq[T]]
        case (_, Invalid(b)) =>
          b.invalid[Seq[T]]
      }
    }
  }
}
