package co.task.entities

case class Client(name: String, dollarBalance: Int, stockNameToAmount: Map[String, Int]) {
  def decreaseBalance(cnt: Int) = {
    val newBalance = dollarBalance - cnt
    copy(dollarBalance = newBalance)
  }

  def increaseBalance(cnt: Int) = {
    val newBalance = dollarBalance + cnt
    copy(dollarBalance = newBalance)
  }

  def decreaseStockAmount(stockName: String, cnt: Int) = {
    val newCnt = stockNameToAmount(stockName) - cnt
    copy(stockNameToAmount = this.stockNameToAmount.updated(stockName, newCnt))
  }

  def increaseStockAmount(stockName: String, cnt: Int) = {
    val newCnt = stockNameToAmount(stockName) + cnt
    copy(stockNameToAmount = this.stockNameToAmount.updated(stockName, newCnt))
  }

  def getStockBalance(stockName: String): Int = {
    stockNameToAmount(stockName)
  }
}
