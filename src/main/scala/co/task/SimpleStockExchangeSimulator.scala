package co.task

import co.task.entities.{ Client, Order }
import SimpleStockExchangeSimulator._

class SimpleStockExchangeSimulator {

  private def performExchangeInternal(
    clients:    Map[ClientName, Client],
    stockName:  String,
    price:      Int,
    buyClient:  Client,
    buyOrder:   Order,
    sellClient: Client,
    sellOrder:  Order
  ): (Map[ClientName, Client], Order, Order) = {

    //if client wants to buy more than he can afford
    //then it should be discarded
    val maxBuyAmount = buyClient.dollarBalance / price
    if (maxBuyAmount < buyOrder.amount) {
      return (clients, buyOrder, sellOrder)
    }

    //if client wants to sell more than he can afford
    //then it should be discarded as well
    val maxSellAmount = sellClient.stockNameToAmount(stockName)
    if (maxSellAmount < sellOrder.amount) {
      return (clients, buyOrder, sellOrder)
    }

    val exchangeCnt = Math.min(buyOrder.amount, sellOrder.amount)
    val updatedOrd1 = buyOrder.decreaseAmount(exchangeCnt)
    val updatedOrd2 = sellOrder.decreaseAmount(exchangeCnt)
    val updatedBuyCl = buyClient
      .decreaseBalance(exchangeCnt * price)
      .increaseStockAmount(stockName, exchangeCnt)
    val updatedSellCl = sellClient
      .increaseBalance(exchangeCnt * price)
      .decreaseStockAmount(stockName, exchangeCnt)
    val updatedClients = clients
      .updated(buyClient.name, updatedBuyCl)
      .updated(sellClient.name, updatedSellCl)
    (updatedClients, updatedOrd1, updatedOrd2)
  }

  def performExchange(
    clients: Map[ClientName, Client],
    order1:  Order,
    order2:  Order
  ): (Map[ClientName, Client], Order, Order) = {

    require((order1.orderType.isSell && order2.orderType.isBuy) ||
      (order1.orderType.isBuy && order2.orderType.isSell))
    require(order1.stockName == order2.stockName &&
      order1.price == order2.price)

    val (stockName, price) = (order1.stockName, order1.price)
    val client1 = clients(order1.clientName)
    val client2 = clients(order2.clientName)

    //discard buy/sell operations for self
    if (client1.name == client2.name) {
      return (clients, order1, order2)
    }

    if (order1.orderType.isBuy) {
      performExchangeInternal(
        clients, stockName, price, client1, order1, client2, order2
      )
    } else {
      val (m, a, b) = performExchangeInternal(
        clients, stockName, price, client2, order2, client1, order1
      )
      //is order to preserve the initial order
      (m, b, a)
    }
  }

  def processSingleOrder(
    order:   Order,
    clients: Map[ClientName, Client],
    orders:  Vector[Order]
  ): (Vector[Order], Map[ClientName, Client]) = {

    @scala.annotation.tailrec
    def processSingleOrderInternal(
      order:     Order,
      clients:   Map[ClientName, Client],
      orders:    Vector[Order],
      accOrders: Vector[Order]           = Vector.empty[Order]
    ): (Vector[Order], Map[ClientName, Client]) = {
      if (orders.isEmpty) {
        (accOrders, clients)
      } else {
        val curOrder = orders.head
        val tail = orders.tail
        val (updatedClients, updatedCurOrder, updatedOrder) = {
          val differentTypes = (curOrder.orderType.isBuy && order.orderType.isSell) ||
            (curOrder.orderType.isSell && order.orderType.isBuy)
          if (curOrder.price == order.price && curOrder.stockName == order.stockName && differentTypes) {
            performExchange(clients, curOrder, order)
          } else {
            (clients, curOrder, order)
          }
        }
        processSingleOrderInternal(updatedOrder, updatedClients, tail, accOrders :+ updatedCurOrder)
      }
    }

    processSingleOrderInternal(order, clients, orders)
  }

  def processOrders(clients: Seq[Client], orders: Seq[Order]): Seq[Client] = {

    @scala.annotation.tailrec
    def processOrdersInternal(
      clients: Map[ClientName, Client],
      orders:  Vector[Order]
    ): Map[ClientName, Client] = {

      if (orders.isEmpty) {
        clients
      } else {
        val curOrder = orders.head
        val tail = orders.tail
        val (updOrders, updClients) = {
          processSingleOrder(curOrder, clients, tail)
        }
        processOrdersInternal(updClients, updOrders.filter(_.amount > 0))
      }
    }

    processOrdersInternal(clients.map(x => (x.name, x)).toMap, orders.toVector).values.toSeq
  }
}

object SimpleStockExchangeSimulator {
  type StockName = String
  type ClientName = String
}
