# Simple stock exchange simulator

### In order to run tests:
    > sbt test
    
### In order to run app:
    > sbt "run <clients_path> <orders_path> <output_path>"
    
    for example:
    sbt "run data/clients.txt data/orders.txt results.txt"
