package co.task

import cats.data.NonEmptyList
import co.task.parser.Parser
import co.task.parser.Parser.ParsingResult
import co.task.parser.RawDataSource.StringSource
import co.task.TestUtils._

class ParserSpec extends BaseSpec {

  "parser" must {
    "parse input[1] (success case)" in {
      val clients =
        "C1\t1000\t130\t240\t760\t320\n" +
          "C2\t2000\t150\t340\t160\t0"
      val orders =
        "C2\tb\tC\t15\t4\n" +
          "C1\ts\tC\t15\t4\n" +
          "C1\ts\tC\t15\t5\n"

      val parsingResult = getParsingResult(clients, orders)
        .right.toOption.getMandatory
      parsingResult.clients.size shouldBe 2
      parsingResult.orders.size shouldBe 3
    }

    "parse input[2] (success case)" in {
      val clients = ""
      val orders = ""

      val parsingResult = getParsingResult(clients, orders)
        .right.toOption.getMandatory
      parsingResult.clients.size shouldBe 0
      parsingResult.orders.size shouldBe 0
    }

    "parse input[3] (failed case)" in {
      val clients = "c1\nc2\nc3"
      val orders = "c1\nc2\nc3"

      val failedParsingResult = getParsingResult(clients, orders)
        .left.toOption.getMandatory
      failedParsingResult.toList.size shouldBe 6
    }
  }

  def getParsingResult(rawClients: String, rawOrders: String): Either[NonEmptyList[Parser.ParsingError], ParsingResult] = {
    val clientSrc = StringSource(rawClients)
    val ordersSrc = StringSource(rawOrders)
    val parser = new Parser(clientSrc, ordersSrc)
    parser.parse().toEither
  }
}
